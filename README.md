
# Instructions for deploying automatically to EC2 via Gitlab CI

Configure CI environment variables:

- SSH_PRIVATE_KEY: Private key to access the ec2 instance.
- EC2_INSTANCE: connection url for ssh connection (eg. "ubuntu@22.22.22.22").
- SSH_FINGERPRINT: Fingerprint of SSH key to verify that the EC2 host is correct one.

Gitlab CI will set up the environment for connecting to EC2 using before_script in deploy job.
Then ssh connection is made to the instance and we fetch the correct commit (using Gitlab global variable CI_COMMIT_SHA) and
run docker-compose up -d to start the containers.

The live environment is available at: http://ec2-13-48-49-98.eu-north-1.compute.amazonaws.com:8080/
