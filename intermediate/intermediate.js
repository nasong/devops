let sendTask = require('./rabbit-utils/sendTask.js');
let receiveTask = require('./rabbit-utils/receiveTask.js');
let http = require("http");
const host = 'rabbitmq';

let healthy = false

// Intermediate: receive a message from topic "my.o"
// publish message to "my.i"
const IMED = async function IMED_func() {
    receiveTask.getTask(
        host,
        "my.o", 
        function (msg) {
            sendTask.addTask(host, "my.i", `Got ${msg.content.toString()}`);
        },
        () => { healhty = true }
    )
}

IMED();


http.createServer(function (req, res) {
    if (healhty) {
        res.statusCode = 200
        res.end()
    } else {
        res.statusCode = 500
        res.end()
    }

}).listen(8001);
