let sendTask = require('./rabbit-utils/sendTask.js');
const host = 'rabbitmq';
const http = require('http');

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
 }

const ORIG = async function ORIG_func() {
    for (i = 1; i < 4; i++) {
        msg = `MSG_${i}`
        sendTask.addTask(host, "my.o", msg);
        wait(3000);
    }
}

ORIG();

http.createServer(function(req, res) {

    res.end()

}).listen(8002);