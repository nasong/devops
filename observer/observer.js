let sendTask = require('./rabbit-utils/sendTask.js');
let receiveTask = require('./rabbit-utils/receiveTask.js');
const host = 'rabbitmq';
const http = require('http');
const fs = require('fs');
let healthy = false;
const path = '/observer/OBSE_result.txt'

fs.access(path, fs.constants.F_OK, (err) => {
    if (err) {
        console.log("File with the same name does not exist. New file will be created.");
    }
    else {
        fs.unlink(path, function(err) {
            if(err && err.code == 'ENOENT') {
                // file doens't exist
                console.info("File doesn't exist, won't remove it.");
            } else if (err) {
                // other errors, e.g. maybe we don't have enough permission
                console.error("Error occurred while trying to remove file");
            } else {
                console.log("OBSE function is being run again. The existing file is removed first.")
            }
        });
    }
}) 

// Observer: receive a message from any topics
const OBSE = async function OBSE_func() {
    let date = new Date();
    let timestamp = date.toISOString()
    receiveTask.getTask(
        host, 
        "#", 
        function (msg) {
            let topic = msg.fields.routingKey;
            let message = msg.content.toString();
            let row = `${timestamp} Topic ${topic}:${message} \n`
            fs.appendFile(path, row, {flag: "a+"}, function (err) {
                if (err) {
                  // append failed
                  console.log("error occured in writing file", err)
                } else {
                  console.log("line added to the file in docker volume")
                }
              })
        },
        () => { healhty = true }
    )
}

OBSE();


http.createServer(function(req, res) {
    if (healthy) {
        res.statusCode = 200;
        res.end()
    } else {
        res.statusCode = 500;
        res.end();
    }
}).listen(8000);