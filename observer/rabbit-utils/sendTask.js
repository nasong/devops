let amqp = require('amqplib/callback_api');

module.exports.addTask = function(rabbitHost, key, msg) {
    amqp.connect(`amqp://${rabbitHost}`, function (err0, connection) {
        if (err0) {
            throw err0
        }

        connection.createChannel(function (err1, channel) {
            if (err1) {
                throw err1
            }

            var exchange = 'topic_logs'

            channel.assertExchange(exchange, 'topic', {
                durable: false
            });

            channel.publish(exchange, key, Buffer.from(JSON.stringify(msg)));
            console.log(" [x] Sent %s:'%s'", key, msg);
        });

    })
}