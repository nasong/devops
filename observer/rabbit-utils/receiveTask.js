let amqp = require('amqplib/callback_api');

module.exports.getTask = function (rabbitHost, key, callback, healthy_callback) {
    amqp.connect(`amqp://${rabbitHost}`, function (err0, connection) {
        
        if (err0) {
            throw err0;
        }

        connection.createChannel(function (err1, channel) {
            if (err1) {
                throw err1;
            }

            var exchange = 'topic_logs'

            channel.assertExchange(exchange, 'topic', {durable: false});

            channel.assertQueue('', { exclusive: true }, function (err2, q) {
                if (err2) {
                    throw err2;
                }

                console.log(' [*] Waiting for logs. To exit press CTRL+C')
                channel.bindQueue(q.queue, exchange, key);
                healthy_callback()
                channel.consume(q.queue, function (msg) {
                    console.log(" [x] %s:'%s'", msg.fields.routingKey, msg.content.toString());
                    callback(msg)
                }, { noAck: true });
            });
        })
    })
};

