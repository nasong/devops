const fs = require("fs");
let http = require("http");

http.createServer(function(req, res) {
    let filePath = '/observer/OBSE_result.txt';
    fs.access(filePath, fs.constants.F_OK, function(err) {
        if (err) {
            res.writeHead(400, {'Content-Type': 'text/plain'});
            res.end("ERROR! File does not exist");
        }
        else {
            var stat = fs.statSync(filePath);

            res.writeHead(200, {
                'Content-Type': 'text/plain',
                'Content-Length': stat.size
            });

            let readStream = fs.createReadStream(filePath);
            readStream.pipe(res);
        }
    })
    

}).listen(8080);